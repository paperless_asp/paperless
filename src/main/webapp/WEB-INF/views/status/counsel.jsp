<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	setDefaultDate($("#defaultSearchDate option:selected").val());
	getProgressStatus();
	getViewAjax();
	
	$("#pageSize").bind("change",function(){
		getViewAjax();
	});

	$("#defaultSearchDate").bind("change",function(){
		setDefaultDate($("#defaultSearchDate option:selected").val());	
	});
	
	$("#searchButton").bind("click",function(){
		getViewAjax();
	});
	
	$("#confirm").bind("click",function(){
		if($("#progress").prop("checked")){
			if(confirm("해당건을 담당하시겠습니까?")){
				updateProgressAdmin($("#progressId").text());
				getViewAjax();
			}
		}
		$(".layerpop_wrap").attr("style","display:none;");
	});
});


/* 
* 
*/
function getViewAjax(){
	var pageSize = $("#pageSize option:selected").val();
	var pageNo = $("#pageNo").val();
	var totalCnt = 0;
	var sdate = $("#sdate").val() + " 00:00:00";
	var edate = $("#edate").val() + " 23:59:59";
	var progress_status = $("#progress_status option:selected").val();
	var inquiry_credit_rate = $("#inquiry_credit_rate option:selected").val();
	var method="POST";
	var requestUrl="/${map.loanId}/api/counselSelectList";
	var params = {
		"pageNo": pageNo, 
		"pageSize": pageSize,
		"sdate":sdate,
		"edate":edate,
		"progress_status":progress_status,
		"inquiry_credit_rate":inquiry_credit_rate,
		"submenu":"${map.submenu}"
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setView);
}


/* 
* list / paging / count 세팅
*/
function setView(response){

	var pageBlock = 10;
	var pageSize = $("#pageSize option:selected").val();
	var pageNo = $("#pageNo").val();
	var stateNo = (pageNo-1) * pageSize;
	if (response.result) {
		$(".lst_table01 tbody").empty();
		var inquiry_credit_rate_name = "";
		$(response.list).each(function(k,v){
			totalCnt = v.totalCnt;
			
			v.inquiry_credit_rate==1?inquiry_credit_rate_name = "진행":inquiry_credit_rate_name = "미진행"; 
			
			$(".lst_table01 tbody").append(
				"<tr>"+
					"<td>"+(v.totalCnt-stateNo)+"</td>"+
					"<td><a href=\"javascript:getProgressDeatilAjax('"+ v.id +"')\">"+v.id+"</a></td>"+
					"<td>대기</td>"+
					"<td>"+v.user_name+"O</td>"+
					"<td>"+v.require_amount+"만원</td>"+
					"<td>"+inquiry_credit_rate_name+"</td>"+
					"<td>"+v.call_start_time+"시~"+v.call_end_time+"시</td>"+
					"<td>"+v.modify_date+"</td>"+
				"</tr>"
			);
			stateNo = stateNo + 1;
		});
		//카운트
		$(".txt").text("총 "+totalCnt+"건의 결과가 검색되었습니다.");
		//페이징 세팅
		var html = setPaging(pageNo , pageSize , pageBlock , totalCnt);
		$("#paging").empty();
		$("#paging").append(html);
	} else {
		if(response.message != "")
			alert(response.message);
		if(response.url != "")
			window.location.href=response.url;
	}
}

function getProgressDeatilAjax(progressId){
	var method="POST";
	var requestUrl="/${map.loanId}/api/counselSelectOne";
	var params = {
		"progressId": progressId
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setProgressView);
}

function setProgressView(response){
	if(response.result){
		$("#progressId").text(response.map.id);
		$("#user_name").text(response.map.user_name+"O");
		$("#require_amount").text(response.map.require_amount+"만원");
		$("#reg_date").text(response.map.reg_date);
		$("#tel_number").text(response.map.tel_number);
		$("#call_time").text(response.map.call_time);
		$(".layerpop_wrap").attr("style","display:block;");
	}else{
		if(response.message != "")
			alert(response.message);
		if(response.url != "")
			window.location.href=response.url;
	}
}

function updateProgressAdmin(progressId){
	var method="POST";
	var requestUrl="/${map.loanId}/api/progressAdminUpdate";
	var params = {
		"progressId": progressId,
		"progress_status":2 
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , resultData);
}

function getProgressStatus(){
	var method="POST";
	var requestUrl="/${map.loanId}/api/statusCodeSelectList";
	var params = {
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setProgressStatus);
}

function setProgressStatus(response){
	if (response.result) {
		$("#progress_status").empty();
		$("#progress_status").append("<option value=''>진행 상태 전체</option>");
		$(response.list).each(function(k,v){
			$("#progress_status").append("<option value='"+v.progress_status+"'>"+ v.status_name +"</option>");
		});
	} else {
		if(response.message != "")
			alert(response.message);
		if(response.url != "")
			window.location.href=response.url;
	}
}
</script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->

	<!-- container -->
	<div id="container">
		<!-- sub menu -->
		<%@ include file="/WEB-INF/views/include_web/sub_menu.jsp"%>
		<!-- hidden -->
		<input type="hidden" id="pageNo" value="1" />
		<!-- sub search -->
		<div class="sub_topsearch">
			<ul class="clearbox">
				<li>
					<select name="" id="defaultSearchDate" class="select01">
						<option value="7">최근 7일</option>
						<option value="14">최근 14일</option>
						<option value="30">최근 1개월</option>
						<option value="">직접입력</option>
					</select>
				</li>
				<li>
					<span class="sch_data"><input type="text" class="datepicker" id="sdate"></span>
					~
					<span class="sch_data"><input type="text" class="datepicker" id="edate"></span>
				</li>
				<li>
					<select name="" id="progress_status" class="select01" style="width:153px;">
						<option value="">진행 상태 전체</option>
					</select>
				</li>
				<li>
					<select name="" id="inquiry_credit_rate" class="select01" style="width:153px;">
						<option value="">신용 조회 전체</option>
						<option value="0">미진행</option>
						<option value="1">진행</option>
					</select>
				</li>
				<li><button type="button" class="btn_st bt01" id="searchButton"><span>검색</span></button></li>
			</ul>
		</div>

		<!-- top btn -->
		<div class="top_area clearbox">
			<div class="lf_box">
				<strong class="txt"></strong>
			</div>
			<div class="rf_box">
				<select name="pageSize" id="pageSize" class="select02" style="width:92px;">
					<option value="20">20개</option>
					<option value="50">50개</option>
					<option value="100">100개</option>
				</select>
			</div>
		</div>

		<!-- contents -->
		<div class="contents">
			<!-- lst_table01 -->
			<div class="lst_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>상담신청 리스트</caption>
	                <colgroup>
		                <col style="width:7%;">
		                <col style="width:12%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:12%;">
		                <col style="width:10%;">
		                <col style="width:15%;">
		                <col style="width:auto;">
	                </colgroup>
	                <thead>
						<tr>
							<th class="num">No</th>
							<th>관리 코드</th>
							<th>진행 상태</th>
							<th>고객명</th>
							<th>신청금액</th>
							<th>신용 조회</th>
							<th>희망 상담 시간</th>
							<th>상담신청일시</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
			<!-- //lst_table01 -->

			<!-- paging -->
			<div class="paging" id="paging"></div>
			<!-- //paging -->
		</div>

	</div>
	<!-- //container -->

</div>

<!-- layerpopup 대출신청 상세정보 -->
<div class="layerpop_wrap" style="display:none;">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:360px; top:150px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">대출신청 상세정보</div>

			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
			        <caption>관리코드</caption>
			        <colgroup>
				        <col style="width:38%;">
				        <col style="width:62%;">
			        </colgroup>
					<tbody>
						<tr>
							<th>관리코드</th>
							<td id="progressId">A123454</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">상담 신청 정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>상담 신청 정보</caption>
	                <colgroup>
		                <col style="width:38%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>고객명</th>
							<td id="user_name">홍길동</td>
						</tr>
						<tr>
							<th>대출신청금액</th>
							<td id="require_amount">500만원</td>
						</tr>
						<tr>
							<th>신청일</th>
							<td id="reg_date">2016.11.20 15:20:15</td>
						</tr>
						<tr>
							<th>연락처</th>
							<td id="tel_number">010-○○○○-4567</td>
						</tr>
						<tr>
							<th>연락희망시간</th>
							<td id="call_time">13시~14시</td>
						</tr>
						<tr>
							<th>신용조회여부</th>
							<td>진행(1등급, 최대 6.8%)</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<p class="stxt02">위 고객의 신청 정보를 확인했으며, <br>
			상담 진행 및 대출 신청 관련 업무를 진행합니다.</p>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>상담 신청 정보</caption>
	                <colgroup>
		                <col style="width:10%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th><input type="checkbox" id="progress" name="progress"></th>
							<td><label for="progress">본 건을 담당해 진행합니다.</label></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->
			
			<p class="stxt01">※선택하지 않을 경우, 다른 담당자가 확인 후 진행할 수 있습니다.</p>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="confirm"><span>확인</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 대출신청 상세정보 -->


</body>
</html>