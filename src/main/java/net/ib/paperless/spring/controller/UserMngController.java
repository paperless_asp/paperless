package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/{loanId}/user_mng/*")
public class UserMngController {

	@RequestMapping(value="/user_mng",method={RequestMethod.GET , RequestMethod.POST})
	public String loan(Model model , @PathVariable String loanId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","user_mng");
		map.put("topmenu","user_mng");
		model.addAttribute("map", map);
		return "/user_mng/user_mng";
	}
}