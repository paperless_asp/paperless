package net.ib.paperless.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//import net.ib.paperless.spring.service.DbService;

@Controller
public class WebController {
	//@Autowired
    //DbService dbService;

	@RequestMapping({"/", "/index", "/dashboard"})
	public String index(Model model) {
		model.addAttribute("menu", "dashboard");
		return "redirect:/sanwa/status/loan";
		//return "index.tiles";
	}
	
	@RequestMapping("/empty")
	public String empty(Model model) {
		//System.out.println("test : " + dbService.getDual());
		model.addAttribute("menu", "empty");
		return "empty.tiles";
	}
	
	@RequestMapping("/login")
	public String login(Model model , HttpServletRequest request) {
	    String referrer = request.getHeader("Referer");
	    System.out.println("referrer : " + referrer);
	    request.getSession().setAttribute("prevPage", "/abc/status/loan");
	    //System.out.println("request.getSession() "  + request.getSession().getAttribute("prevPage"));
//		model.addAttribute("base", "http://localhost:9080/");
		return "login";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model) {
		//model.addAttribute("menu", "empty");
		return "detail";
	}

	
	/*@RequestMapping("/error")
	public String error(Model model) {
		//model.addAttribute("menu", "empty");
		return "redirect:/sanwa/status/loan";
	}*/
}
