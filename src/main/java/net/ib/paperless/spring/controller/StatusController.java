package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/{loanId}/status/*")
public class StatusController {
	
	@RequestMapping(value="/counsel",method={RequestMethod.GET , RequestMethod.POST})
	public String counsel(Model model , @PathVariable String loanId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","counsel");
		map.put("topmenu","status");
		model.addAttribute("map", map);
		return "/status/counsel";
	}

	@RequestMapping(value="/loan",method={RequestMethod.GET , RequestMethod.POST})
	public String loan(Model model , @PathVariable String loanId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","loan");
		map.put("topmenu","status");
		model.addAttribute("map", map);
		return "/status/loan";
	}
}