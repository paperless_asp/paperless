package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/{loanId}/stats/*")
public class StatsController{
	
	@RequestMapping("/id")
	public String id(Model model , @PathVariable String loanId){

		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","id");
		map.put("topmenu","stats");
		model.addAttribute("map", map);
		return "/stats/id";
	}

	
	@RequestMapping("/month")
	public String month(Model model , @PathVariable String loanId){

		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","month");
		map.put("topmenu","stats");
		model.addAttribute("map", map);
		return "/stats/month";
	}

	
	@RequestMapping("/year")
	public String year(Model model , @PathVariable String loanId){

		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","year");
		map.put("topmenu","stats");
		model.addAttribute("map", map);
		return "/stats/year";
	}
}