package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/{loanId}/admin_mng/*")
public class AdminMngController{
	
	@RequestMapping(value="/prod_mng",method={RequestMethod.GET , RequestMethod.POST})
	public String prodMng(Model model ,@PathVariable String loanId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","prod_mng");
		map.put("topmenu","admin_mng");
		model.addAttribute("map", map);
		return "/admin_mng/prod_mng";
	}
}