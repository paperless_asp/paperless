package net.ib.paperless.spring.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.ib.paperless.spring.service.AdminNoticeService;
import net.ib.paperless.spring.service.AuthenticationService;

@Controller
@RequestMapping("/{loanId}/admin_mng/*")
public class AdminNoticeController{

	@Autowired
	AuthenticationService authenticationService;
	
	@RequestMapping(value="/notice",method={RequestMethod.GET , RequestMethod.POST})
	public String notice(Model model ,@PathVariable String loanId, Principal principal){
		System.out.println("principal = "+principal);
		
		Integer admin_level = authenticationService.userLevelSelectOne(principal.getName());
		
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);
		map.put("submenu","notice");
		map.put("topmenu","admin_mng");
		map.put("admin_name", principal.getName());
		map.put("admin_level", admin_level);
		System.out.println("map = "+map);
		model.addAttribute("map", map);
		return "/admin_mng/notice";
	}
}
