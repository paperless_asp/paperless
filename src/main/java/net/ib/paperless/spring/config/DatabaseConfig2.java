package net.ib.paperless.spring.config;
/*
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
@MapperScan(basePackages = "net.ib.paperless.spring.dao")
public class DatabaseConfig {
    @Value("${spring.dataSource.driver-class-name}") private String driverClassName;
    @Value("${spring.dataSource.url}") private String url;
    @Value("${spring.dataSource.username}") private String username;
    @Value("${spring.dataSource.password}") private String password;
    
	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
		final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setMapperLocations(resolver.getResources("classpath:dao/*.xml"));
		return sessionFactory.getObject();
	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
		final SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
		return sqlSessionTemplate;
	}
    
	@Bean(destroyMethod = "close")
	public DataSource dataSource() {
        
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("org.mariadb.jdbc.Driver");
		dataSource.setUrl("jdbc:mariadb://39.117.41.170:3306/paperless");
		dataSource.setUsername("paperlessdev");
		dataSource.setPassword("paperless123!@#");
		dataSource.setDefaultAutoCommit(false);
        return dataSource;
	}

}*/