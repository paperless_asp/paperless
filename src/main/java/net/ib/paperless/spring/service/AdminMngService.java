package net.ib.paperless.spring.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clipsoft.org.json.simple.parser.JSONParser;
import com.clipsoft.org.json.simple.parser.ParseException;

import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.ProdMng;
import net.ib.paperless.spring.repository.AdminMngRepository;

@Service
public class AdminMngService {
	@Autowired 
	AdminMngRepository adminMngRepository;
	
	public List<ProdMng> prodMngSelectList(Map params){
		int pageSize = 0;
		int pageNo = 0;
		int pagePoint = 0;
		
		if(params.get("pageSize") == null) pageSize = 20;
		else pageSize = Integer.parseInt(params.get("pageSize").toString());
		
		if(params.get("pageNo") == null) pageNo = 1;
		else pageNo = Integer.parseInt(params.get("pageNo").toString());
		
		pagePoint = (pageNo - 1) * pageSize;
		
		params.put("pageSize", pageSize);
		params.put("pageNo", pageNo);
		params.put("pagePoint", pagePoint);
		System.out.println("params : " + params);
		return adminMngRepository.prodMngSelectList(params);
	}

	public ProdMng prodMngSelectOne(int seq){
		return adminMngRepository.prodMngSelectOne(seq);
	}
	
	public List<EformAttach> eformAttachSelectList(int seq){
		return adminMngRepository.eformAttachSelectList(seq);
	}
	
	public int eformAttachInsert(Map map){
		return adminMngRepository.eformAttachInsert(map);
	}
	
	public List<LoanInfo> loanInfoSelectList(){
		return adminMngRepository.loanInfoSelectList();
	}
	
	public int eformInfoInsert(Map map){

		map.put("attach_type_count", 0);
		map.put("seq", 0);
		adminMngRepository.eformInfoInsert(map);
		System.out.println("map  :  " + map);
		int seq = Integer.parseInt(map.get("seq").toString());
		if(seq > 0){
			if(map.get("eform_attachs") != null){
				List<String> eformAttachs = (List<String>) map.get("eform_attachs");
				System.out.println("efromAttachs : " + eformAttachs);
				boolean boo = true;
				if(!eformAttachs.isEmpty()){
					//foreach(;)
					for(String s : eformAttachs){
						Map<String,Object> nMap = new HashMap<String,Object>();
						nMap.put("eform_id", seq);
						nMap.put("name", s);
						nMap.put("eform_path", "");
						System.out.println("nMap: " + nMap);
						if(adminMngRepository.eformAttachInsert(nMap) <= 0){
							boo = false;
							break;
						}
					}
					if(boo)
						return 1;
					else
						return 0;
				}else
					return 0;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		//return adminMngRepository.eformInfoInsert(map);
	}
	

	
	public int eformInfoInsertAsMultipart(Map map){

		String[] eformAttachsArray = map.get("eform_attachs").toString().split(",");
		List<String> eformAttachs = new ArrayList(Arrays.asList(eformAttachsArray));
		map.put("attach_type_count", eformAttachs.size());
		map.put("seq", 0);
		adminMngRepository.eformInfoInsert(map);
		System.out.println("map  :  " + map);
		int seq = Integer.parseInt(map.get("seq").toString());
		if(seq > 0){
			if(eformAttachs.size() > 0){
				System.out.println("efromAttachs : " + eformAttachs);
				boolean boo = true;
				if(!eformAttachs.isEmpty()){
					//foreach(;)
					for(String s : eformAttachs){
						Map<String,Object> nMap = new HashMap<String,Object>();
						nMap.put("eform_id", seq);
						nMap.put("name", s);
						nMap.put("eform_path", "");
						System.out.println("nMap: " + nMap);
						if(adminMngRepository.eformAttachInsert(nMap) <= 0){
							boo = false;
							break;
						}
					}
					if(boo)
						return 1;
					else
						return 0;
				}else
					return 0;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		//return adminMngRepository.eformInfoInsert(map);
	}

	public int eformInfoUpdateAsMultipart(Map map) throws ParseException{

		adminMngRepository.eformInfoUpdate(map);
		
		String eformAttachsJson = (String) map.get("eform_attachs");
		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(eformAttachsJson);
		List<Object> list = (List<Object>) obj;
		if(list.size() > 0){
			boolean boo = true;
			for(Object s : list){
				Map<String,Object> o = (Map<String, Object>) s;
				o.put("eform_path", "");
				if(adminMngRepository.eformAttachUpdate(o) <= 0){
					boo = false;
					break;
				}
			}
			
			if(boo) return 1;
			else return 0;
		}else{return 0;}
	}
	
	public int eformDelete(HashMap<String,Object> params){
		System.out.println("params : " + params.get("eformDel"));
		List<String> list = (List<String>)params.get("eformDel");
		System.out.println("list : " + list);
		for(String i : list){
			adminMngRepository.eformInfoDelete(Integer.parseInt(i));
			adminMngRepository.eformAttachDelete(Integer.parseInt(i));
		}
		return 1;
	}
}