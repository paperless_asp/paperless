<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="../../include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="../../include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../../include/js/common.js"></script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<div id="header">
		<h1><a href="#">대출신청확인</a></h1>
		<a href="#" class="back">back</a>
		<div class="gnb_menu">
			<p>side menu</p>
			<span class="lt"></span>
			<span class="rt"></span>
			<span class="lb"></span>
			<span class="rb"></span>
		</div>
	</div>
	<!-- //header -->
	<!-- gnb -->
	<div class="gnb_mask"></div>
	<nav id="gnb">
		<ul>
			<li><a href="#" class="menu01">신용/한도 조회</a></li>
			<li><a href="#" class="menu02">대출신청내역 조회</a></li>
			<li class="line"><span></span></li>
			<li><a href="#">고객문의</a></li>
			<li><a href="#">공지사항</a></li>
		</ul>
	</nav>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="top_infotxt">
			모바일 대출신청을 선택해 <span>상세정보를 확인</span>하세요.
		</div>	

		<div class="contbox bg">
			<h2 class="stit">고객정보</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>신청자 기본정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">이름</th>
			            <td>홍길동</td>
			        </tr>
			        <tr>
			            <th scope="row">주민등록번호</th>
			            <td>750111-*******</td>
			        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="contwbox bg tline">
			<h2 class="stit">모바일 대출 신청 내역</h2>
			<!-- lst_table -->
			<div class="lst_table">
			    <table cellspacing="0" border="1">
			        <caption>대출 신청 내역</caption>
			        <colgroup>
			            <col style="width:33%;">
			            <col style="width:33%;">
			            <col style="width:auto;">
			        </colgroup>
			        <thead>
				        <tr>
				            <th scope="col">신청일</th>
				            <th scope="col">대출금액</th>
				            <th scope="col">진행상태</th>
				        </tr>
			        </thead>
			        <tbody>
				        <tr>
				            <td>2016.11.15</td>
				            <td>1,000만원</td>
				            <td>대출심사중</td>
				        </tr>
				        <tr>
				            <td>2016.11.15</td>
				            <td>500만원</td>
				            <td>서류작성중</td>
				        </tr>
				        <tr>
				            <td colspan="3">신청 내역이 존재하지 않습니다.</td>
				        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //lst_table -->

			<ul class="lnc_txt lrm">
				<li>※ 서류 작성 중인 경우, 빠른 시간 내 관련 서류 제출 완료가 필요합니다.</li>
				<li>※ 신청 완료 후 서류 검토 과정에서 보완을 요청한 경우, 관련 서류를 확인한 후 양식에 맞게 다시 제출해야 합니다.</li>
			</ul>
		</div>

		

	</div>
	<!-- //contents -->
</div>
</body>
</html>