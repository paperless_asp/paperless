<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	setDefaultDate($("#defaultSearchDate option:selected").val());
	getProgressStatus();
	getViewAjax();

	$("#pageSize").bind("change",function(){
		getViewAjax();
	});
	
	$("#defaultSearchDate").bind("change",function(){
		setDefaultDate($("#defaultSearchDate option:selected").val());	
	});
	
	$("#searchButton").bind("click",function(){
		getViewAjax();
	});
	
	$("#progressDetailClose").bind("click",function(){
		$("#progressDetail").attr("style","display:none;");	
	});
	
	$("#progressDetail_body_add").bind("click",function(){
		if($("#progressDetail_body").val() == ""){
			alert("내용을 확인해주십시오.");
			$("#progressDetail_body").focus();
		}else{
			addProgressNote();
		}
	});
	
	$("#amount_change_button").bind("click" , function(){
		$("#require_amount_layer").attr("style","display:block;");
	});
	
	$("#require_amount_layer_close").bind("click",function(){
		$("#require_amount_layer").attr("style","display:none;");
	});
	
	$("#require_amount_layer_submit").bind("click",function(){
		requireAmountUpdate();
	});
	
	$("#call_change_button").bind("click",function(){
		$("#call_yn_layer").attr("style","display:block;");
	});

	$("#call_yn_close").bind("click",function(){
		$("#call_yn_layer").attr("style","display:none;");
	});

	$("#call_yn_submit").bind("click",function(){
		callYnUpdate();
	});
	
	$("#eform_attach_button").bind("click",function(){
		$("#attach_yn_layer").attr("style","display:block;");
		var progressId = $("#progressId").text();
		var method="POST";
		var requestUrl="/${map.loanId}/api/eformAttachSelectList";
		var params = {
			"progressId": progressId
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		$.ajax({
			url: requestUrl,
			type: method,
			data: JSON.stringify( params),
			dataType: getType,
			contentType : contType,
			cache: false,
			success: function(response) {
				var progress_status = "";
				if (response.result) {
					$("#eform_attach_list").empty();
					$(response.list).each(function(k,v){
						$("#eform_attach_list").append("<tr><th><input type=\"checkbox\" id=\"doc01_1\" name=\"doc01\" value=\""+v.seq+"\"></th><td>"+v.name+"</td></tr>");
						progress_status = v.progress_status;
					});
					$("#eform_attach_status").find("option").each(function(k,v){
						if($(v).val() == progress_status){
							$(this).attr("selected","selected");
						}
					});
				}
			},
			fail: function() {
				alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
			}
		});
	});
	
	$("#attach_yn_layer_close").bind("click",function(){
		$("#attach_yn_layer").attr("style","display:none;");
	});
	
	$("#eform_attach_choose_button").bind("click",function(){
		getEformInfoSelectList();
		$("#eform_attach_layer").attr("style","display:block;");
		$("#eform_attach_submit").attr("data-flag" , "edit");
	});
	
	$("#eform_attach_close").bind("click",function(){
		$("#eform_attach_layer").attr("style","display:none;");
	});

	$("#eform_attach_submit").bind("click",function(){
		if(!$("input:radio[name=prod01]:checked").val()){
			alert("상품을 선택해주십시오.");
		}else{
			var dataFlag = $(this).attr("data-flag");
			var eformInfoTitle = $("input:radio[name=prod01]:checked").attr("data-attach-info");
			if(dataFlag == "insert"){
				$("#eform_userdata_insert_eform_info").text(eformInfoTitle);
				$("#eform_userdata_insert_eform_info").attr("data-seq",$("input:radio[name=prod01]:checked").val());
				$("#eform_attach_close").trigger("click");
			}else if(dataFlag == "edit"){
				$("#eform_info_edit").text(eformInfoTitle);
				updateProgress($("input:radio[name=prod01]:checked").val());
				$("#eform_attach_close").trigger("click");
			}else{
				$("#eform_userdata_edit_eform_info").text(eformInfoTitle);
				$("#eform_info_edit").text(eformInfoTitle);
				updateProgress($("input:radio[name=prod01]:checked").val());
				$("#eform_attach_close").trigger("click");
			}
		}
	});
	
	$("#attach_yn_layer_submit").bind("click",function(){
		var eform_attachs = [];
		$("input:checkbox[name=doc01]:checked").each(function(){
			console.log($(this).val());
			eform_attachs.push($(this).val());
		});
		var progressId = $("#progressId").text();
		var progress_status = $("#eform_attach_status option:selected").val();
		var body = $("#attach_yn_note").val();
		var method="POST";
		var requestUrl="/${map.loanId}/api/attachProgressUpdate";
		var params = {
			"body":body,
			"progressId": progressId,
			"eform_attachs": eform_attachs,
			"progress_status": progress_status,
			"admin_confirm_yn":2
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		$.ajax({
			url: requestUrl,
			type: method,
			data: JSON.stringify( params),
			dataType: getType,
			contentType : contType,
			cache: false,
			success: function(response) {
				if (response.result) {
					$("#attach_yn_layer_close").trigger("click");
				}
				alert(response.message);
			},
			fail: function() {
				alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
			}
		});
	});
	
	$("#eform_userdata_edit_button").bind("click",function(){
		$("#edit_user_name").val($("#user_name").text());
		$("#edit_tel_number").val($("#tel_number").text());
		$("#edit_require_amount").val($("#require_amount").text().replace("만원",""));
		
		$("#eform_userdata_edit_eform_info").text($("#eform_info_edit").text());
		$("#eform_userdata_edit_eform_info").attr("data-seq",$("#eform_info_edit").attr("data-seq"));
		
		var progressId = $("#progressId").text();
		var method="POST";
		var requestUrl="/${map.loanId}/api/eformUserDataSelectOne";
		var params = {
			"progressId": progressId
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		$.ajax({
			url: requestUrl,
			type: method,
			data: JSON.stringify(params),
			dataType: getType,
			contentType : contType,
			cache: false,
			success: function(response) {
				if (response.result) {
					$("#edit_eform_userdata_start_date").val(response.map.start_date);
					$("#edit_eform_userdata_start_date_memo").val(response.map.start_date_memo);
					$("#edit_eform_userdata_expire_date").val(response.map.expire_date);
					$("#edit_eform_userdata_expire_date_memo").val(response.map.expire_date_memo);
					$("#edit_eform_userdata_interest_per").val(response.map.interest_per);
					$("#edit_eform_userdata_interest_per_memo").val(response.map.interest_per_memo);
					$("#edit_eform_userdata_interest_day").val(response.map.interest_day);
					$("#edit_eform_userdata_repayment_method").val(response.map.repayment_method);
					$("#edit_eform_userdata_repayment_commission").val(response.map.repayment_commission);
					$("#edit_eform_userdata_loan_method").val(response.map.loan_method);
					$("#edit_eform_userdata_note").val(response.map.note);
				}
			},
			fail: function() {
				alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
			}
		});
		//$("#edit_jumin").val();
		$("#eform_userdata_edit_layer").attr("style","display:block;");
	});
	$("#eform_userdata_edit_close").bind("click",function(){
		$("#eform_userdata_edit_layer").attr("style","display:none;");
	});

	$("#eform_userdata_insert_button").bind("click",function(){
		$("#eform_userdata_insert_layer").attr("style","display:block;");
	});

	$("#eform_userdata_insert_close").bind("click",function(){
		$("#eform_userdata_insert_layer").attr("style","display:none;");
	});
	
	$("#insert_eform_attach_choose_button").bind("click",function(){
		getEformInfoSelectList();
		$("#eform_attach_layer").attr("style","display:block;");
		$("#eform_attach_layer").css("z-index","10");
		$("#eform_attach_submit").attr("data-flag" , "insert");
		$("#eform_userdata_insert_layer").css("z-index","5");
	});
	
	$("#eform_userdata_insert_submit").bind("click",function(){
		var user_name = $("#insert_user_name").val();
		var tel_number = $("#insert_tel_number").val();
		var jumin = $("#insert_jumin").val();
		var bank = $("#insert_bank").val();
		var accouunt_number = $("#insert_accouunt_number").val();
		var eform_info_name = $("#eform_userdata_insert_eform_info").text();
		var eform_info_seq = $("#eform_userdata_insert_eform_info").attr("data-seq");
		var require_amount = $("#insert_require_amount").val();
		var account_number = $("#insert_accouunt_number").val();
		var start_date = $("#insert_eform_userdata_start_date").val();
		var start_date_memo = $("#insert_eform_userdata_start_date").val();
		var expire_date = $("#insert_eform_userdata_expire_date").val();
		var expire_date_memo = $("#insert_eform_userdata_expire_date_memo").val();
		var interest_per = $("#insert_eform_userdata_interest_per").val();
		var interest_per_memo = $("#insert_eform_userdata_interest_per_memo").val();
		var interest_day = $("#insert_eform_userdata_interest_day").val();
		var repayment_method = $("#insert_eform_userdata_repayment_method").val();
		var repayment_commission = $("#insert_eform_userdata_repayment_commission").val();
		var loan_method = $("#insert_eform_userdata_loan_method").val();
		var note = $("#insert_eform_userdata_note").val();
		
		//--ajax
		var method="POST";
		var requestUrl="/${map.loanId}/api/eformUserdataInsert";
		var getType="json";
		var contType="application/json; charset=UTF-8";
		var params = {
			"user_name": user_name, 
			"tel_number": tel_number,
			"jumin":jumin,
			"eform_info_name":eform_info_name,
			"eform_id":eform_info_seq,
			"require_amount":require_amount,
			"account_number":account_number,
			"start_date":start_date,
			"start_date_memo":start_date_memo,
			"expire_date":expire_date,
			"expire_date_memo":expire_date_memo,
			"interest_per":interest_per,
			"interest_per_memo":interest_per_memo,
			"interest_day":interest_day,
			"repayment_method":repayment_method,
			"repayment_commission":repayment_commission,
			"loan_method":loan_method,
			"note":note
		};
		
		if(validation(params)==true){
			$.ajax({
				url: requestUrl,
				type: method,
				data: JSON.stringify( params),
				dataType: getType,
				contentType : contType,
				cache: false,
				success: function(response) {
					if(response.message != "")
						alert(response.message);
					
					if (response.result == true) {
						$("#eform_userdata_insert_close").trigger("click");
						getViewAjax();
					}
				},
				fail: function() {
					alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
				}
			});
		}
		
		//$("#eform_userdata_insert_close").trigger("click");
	});

	$("#eform_userdata_edit_submit").bind("click",function(){
		var user_name = $("#edit_user_name").val();
		var tel_number = $("#edit_tel_number").val();
		var jumin = $("#edit_jumin").val();
		var bank = $("#edit_bank").val();
		var accouunt_number = $("#edit_accouunt_number").val();
		
		var eform_info_name = $("#eform_userdata_edit_eform_info").text();
		var eform_info_seq = $("#eform_userdata_edit_eform_info").attr("data-seq");
		var require_amount = $("#edit_require_amount").val();
		var account_number = $("#edit_accouunt_number").val();
		var start_date = $("#edit_eform_userdata_start_date").val();
		var start_date_memo = $("#edit_eform_userdata_start_date").val();
		var expire_date = $("#edit_eform_userdata_expire_date").val();
		var expire_date_memo = $("#edit_eform_userdata_expire_date_memo").val();
		var interest_per = $("#edit_eform_userdata_interest_per").val();
		var interest_per_memo = $("#edit_eform_userdata_interest_per_memo").val();
		var interest_day = $("#edit_eform_userdata_interest_day").val();
		var repayment_method = $("#edit_eform_userdata_repayment_method").val();
		var repayment_commission = $("#edit_eform_userdata_repayment_commission").val();
		var loan_method = $("#edit_eform_userdata_loan_method").val();
		var note = $("#edit_eform_userdata_note").val();
		
		var progressId = $("#progressId").text();
		
		//--ajax
		var method="POST";
		var requestUrl="/${map.loanId}/api/eformUserdataEdit";
		var getType="json";
		var contType="application/json; charset=UTF-8";
		var params = {
			"user_name": user_name, 
			"tel_number": tel_number,
			"jumin":jumin,
			"eform_info_name":eform_info_name,
			"eform_id":eform_info_seq,
			"require_amount":require_amount,
			"account_number":account_number,
			"start_date":start_date,
			"start_date_memo":start_date_memo,
			"expire_date":expire_date,
			"expire_date_memo":expire_date_memo,
			"interest_per":interest_per,
			"interest_per_memo":interest_per_memo,
			"interest_day":interest_day,
			"repayment_method":repayment_method,
			"repayment_commission":repayment_commission,
			"loan_method":loan_method,
			"note":note,
			"progress_id":progressId,
			"progressId":progressId
		};
		
		if(validation(params)==true){
			$.ajax({
				url: requestUrl,
				type: method,
				data: JSON.stringify( params),
				dataType: getType,
				contentType : contType,
				cache: false,
				success: function(response) {
					if(response.message != "")
						alert(response.message);
					
					if (response.result == true) {
						$("#eform_userdata_edit_close").trigger("click");
						$("#progressDetailClose").trigger("click");
						getProgressDetailAjax(progressId);
					}
				},
				fail: function() {
					alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
				}
			});
		}
		
		//$("#eform_userdata_insert_close").trigger("click");
	});
	
	$("#edit_eform_attach_choose_button").bind("click",function(){
		getEformInfoSelectList();
		$("#eform_attach_layer").attr("style","display:block;");
		$("#eform_attach_layer").css("z-index","10");
		$("#eform_userdata_edit_layer").css("z-index","5");
		$("#progressDetail").css("z-index","1");
		
		
		$("#eform_attach_submit").attr("data-flag" , "edit2");
	});
});

//대출거래정보 
function validation(params){
	var rtn = false;
	$.each(params,function(key,value){
		if(key == "user_name" && value == ""){
			alert("고객명을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="tel_number" && value == ""){
			alert("연락처를 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="eform_info_name" && value == ""){
			alert("상품명을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="require_amount" && value == ""){
			alert("대출금액을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="account_number" && value == ""){
			alert("계좌번호를 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="start_date" && value == ""){
			alert("여신개시일 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="expire_date" && value == ""){
			alert("여신기간만료일을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="interest_per" && value == ""){
			alert("이자율을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="interest_day" && value == ""){
			alert("이자지급시기를 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="repayment_method" && value == ""){
			alert("상황방법을 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="repayment_commission" && value == ""){
			alert("중도상황/취급수수료를 확인해주십시오.");
			rtn = false;
			return false;
		}else if(key =="loan_method" && value == ""){
			alert("여신실행방법을 확인해주십시오.");
			rtn = false;
			return false;
		}else{
			rtn = true;
		}
	});
	return rtn;
}

function callYnUpdate(){
	var call_yn = $("#call_yn option:selected").val();
	var progressId = $("#progressId").text();
	var body = $("#call_yn_layer_note").val();
	var method="POST";
	var requestUrl="/${map.loanId}/api/progressStatusUpdate";
	var params = {
		"body": body, 
		"progressId": progressId,
		"call_yn":call_yn
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , resultData);
	$('#call_yn_close').trigger('click');
	$("#progressDetailClose").trigger("click");
	getProgressDetailAjax(progressId);
}

/*
 * 대출신청 금액 관리
 */
function requireAmountUpdate(){
	var require_amount = $("#require_amount_value").val();
	var progressId = $("#progressId").text();
	var body = $("#require_amount_layer_note").val();
	var method="POST";
	var requestUrl="/${map.loanId}/api/requireAmountUpdate";
	var params = {
		"body": body, 
		"progressId": progressId,
		"require_amount":require_amount
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , resultData);
	$('#require_amount_layer_close').trigger('click');
	$("#progressDetailClose").trigger("click");
	getProgressDetailAjax(progressId);
}

function addProgressNote(){
	var progressDetail_body = $("#progressDetail_body").val();
	var progressId = $("#progressId").text();
	var method="POST";
	var requestUrl="/${map.loanId}/api/progressNoteInsert";
	var params = {
		"body": progressDetail_body, 
		"progressId": progressId,
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , resultData);
	$("#progressDetail_body").val("");
	getNoteListAjax(progressId);
}

/* 
* 
*/
function getViewAjax(){
	var pageSize = $("#pageSize option:selected").val();
	var pageNo = $("#pageNo").val();
	var totalCnt = 0;
	var sdate = $("#sdate").val() + " 00:00:00";
	var edate = $("#edate").val() + " 23:59:59";
	var progress_status = $("#progress_status option:selected").val();
	var inquiry_credit_rate = $("#inquiry_credit_rate option:selected").val();
	var searchType = $("#searchType option:selected").val();
	var searchText = $("#searchText").val();
	var method="POST";
	var requestUrl="/${map.loanId}/api/counselSelectList";
	var params = {
		"pageNo": pageNo, 
		"pageSize": pageSize,
		"sdate":sdate,
		"edate":edate,
		"searchType":searchType,
		"searchText":searchText,
		"progress_status":progress_status,
		"inquiry_credit_rate":inquiry_credit_rate,
		"submenu":"${map.submenu}"
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setView);
}


/* 
* list / paging / count 세팅
*/
function setView(response){

	var pageBlock = 10;
	var pageSize = $("#pageSize option:selected").val();
	var pageNo = $("#pageNo").val();
	var stateNo = (pageNo-1) * pageSize;
	if (response.result) {
		$(".lst_table01 tbody").empty();
		var inquiry_credit_rate_name = "";
		$(response.list).each(function(k,v){
			totalCnt = v.totalCnt;
			//v.inquiry_credit_rate==1?inquiry_credit_rate_name = "진행":inquiry_credit_rate_name = "미진행"; 
				
			$(".lst_table01 tbody").append(
				"<tr>"+
					"<td>"+(v.totalCnt-stateNo)+"</td>"+
					"<td><a href=\"javascript:getProgressDetailAjax('"+ v.id +"')\">"+v.id+"</a></td>"+
					"<td>"+v.user_name+"O</td>"+
					"<td>"+v.require_amount+"만원</td>"+
					"<td>"+v.status_name+"</td>"+
					"<td>"+v.admin_id+"</td>"+
					"<td>"+v.reg_date+"</td>"+
					"<td>"+v.modify_date+"</td>"+
					"<td>"+v.body+"</td>"+
				"</tr>"
			);
			stateNo = stateNo + 1;
		});
		//카운트
		$(".txt").text("총 "+totalCnt+"건의 결과가 검색되었습니다.");
		//페이징 세팅
		var html = setPaging(pageNo , pageSize , pageBlock , totalCnt);
		$("#paging").empty();
		$("#paging").append(html);
	} else {
		alert("실패");
	}
}

function getProgressDetailAjax(progressId){
	var method="POST";
	var requestUrl="/${map.loanId}/api/counselSelectOne";
	var params = {
		"progressId": progressId
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setProgressView);
}

function setProgressView(response){
	var data = response.map;
	
	$("#progressId").text(data.id);
	$("#user_name").text(data.user_name);
	$("#require_amount").text(data.require_amount+"만원");
	$("#require_amount_value").val(data.require_amount);
	$("#reg_date").text(data.reg_date);
	$("#modify_date").text(data.modify_date);
	$("#tel_number").text(data.tel_number);
	$("#call_time").text(data.call_time);
	$("#status_name").text(data.status_name);
	
	//progress_status
	$("#call_yn_txt").text(data.call_yn==0?"미진행":"진행");
	$("#call_chk_date").text(data.call_chk_date==null?"-":data.call_chk_date);
	$("#eform_yn_txt").text(data.eform_yn==0?"미진행":"진행");
	$("#eform_yn_date").text(data.eform_complete_date==null?"-":data.eform_complete_date);

	$("#account_verify_yn_txt").text(data.account_verify_yn==0?"미진행":"진행");
	$("#account_verify_date").text(data.account_verify_date==null?"-":data.account_verify_date);
	$("#attach_yn_txt").text(data.attach_yn==0?"미진행":"진행");
	$("#attach_complete_date").text(data.attach_complete_date==null?"-":data.attach_complete_date);
	
	$("#eform_info_edit").text(data.name);
	$("#eform_info_edit").attr("data-seq",data.eform_id);
	
	//메모 가져오기
	getNoteListAjax(data.id);
	
	$("#progressDetail").attr("style","display:block;");
}

/*
 * 상담이력 Ajax
 */
function getNoteListAjax(progressId){
	var method="POST";
	var requestUrl="/${map.loanId}/api/progressNoteSelectList";
	var params = {
		"progressId": progressId
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setNoteListView);
}

function setNoteListView(response){
	//$("#memo_scroll").append("asdfsadf");
	if (response.result) {
		
		$("#memo_scroll").empty();
		var i=0;
		$(response.list).each(function(k,v){
			$("#memo_scroll").append(
				"<tr>"+
					"<th>"+(Number(v.totalCnt)-i)+"</th>"+
					"<td>"+v.body+"</td>"+
					"<td>"+v.reg_date+"</td>"+
				"</tr>"
			);
			i++;
		});
	} else {
		alert("실패");
	}
}


function getProgressStatus(){
	var method="POST";
	var requestUrl="/${map.loanId}/api/statusCodeSelectList";
	var params = {
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setProgressStatus);
}

function setProgressStatus(response){
	if (response.result) {
		$("#progress_status").empty();
		$("#progress_status").append("<option value=''>진행 상태 전체</option>");
		$(response.list).each(function(k,v){
			$("#progress_status").append("<option value='"+v.progress_status+"'>"+ v.status_name +"</option>");
		});
	} else {
		alert("실패");
	}
}

function getEformInfoSelectList(){
	var method="POST";
	var requestUrl="/${map.loanId}/api/eformInfoSelectList";
	var params = {
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , setEformInfoList);
}

function setEformInfoList(response){
	if (response.result) {
		$("#eFormInfoList").empty();
		var appendHtml = "";
		$(response.list).each(function(k,v){
			appendHtml += "<tr><th><input type=\"radio\" id=\"prod01_"+v.seq+"\" name=\"prod01\" value=\""+v.seq+"\" data-attach-info=\""+v.name+"\"/></th><td>"+v.name+"</td></tr>";
		});

		$("#eFormInfoList").append(appendHtml);
	} else {
		alert("실패");
	}
}

function updateProgress(eform_id){
	var progressId = $("#progressId").text();
	var method="POST";
	var requestUrl="/${map.loanId}/api/progressUpdate";
	var params = {
		"eform_id": eform_id,
		"progressId":progressId
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType ,contType , resultData);
}
</script>

</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->

	<!-- container -->
	<div id="container">
		<!-- sub menu -->
		<%@ include file="/WEB-INF/views/include_web/sub_menu.jsp"%>
		<!-- hidden -->
		<input type="hidden" id="pageNo" value="1" />
		<!-- sub search -->
		<div class="sub_topsearch">
			<ul class="clearbox">
				<li>
					<select name="" id="defaultSearchDate" class="select01">
						<option value="7">최근 7일</option>
						<option value="14">최근 14일</option>
						<option value="30">최근 1개월</option>
						<option value="">직접입력</option>
					</select>
				</li>
				<li>
					<span class="sch_data"><input type="text" class="datepicker" id="sdate"></span>
					~
					<span class="sch_data"><input type="text" class="datepicker" id="edate"></span>
				</li>
				<li>
					<select name="" id="progress_status" class="select01" style="width:153px;">
						<option value="">진행 상태 전체</option>
					</select>
				</li>
				<li>
					<select name="" id="searchType" class="select01" style="width:153px;">
						<option value="user_name">고객명</option>
						<option value="admin_id">상담ID</option>
					</select>
				</li>
				<li class="input_search"><input type="text" name="" id="searchText" placeholder="검색어를 입력하세요"></li>
				<li><button type="button" class="btn_st bt01" id="searchButton"><span>검색</span></button></li>
			</ul>
		</div>

		<!-- top btn -->
		<div class="top_area clearbox">
			<div class="lf_box">
				<strong class="txt"></strong>
			</div>
			<div class="rf_box">
				<select name="pageSize" id="pageSize" class="select02" style="width:92px;">
					<option value="20">20개</option>
					<option value="50">50개</option>
					<option value="100">100개</option>
				</select>
				<button type="button" class="btn_st bt01" id="eform_userdata_insert_button"><span>대출신청 등록</span></button>
			</div> 
		</div>

		<!-- contents -->
		<div class="contents">
			<!-- lst_table01 -->
			<div class="lst_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출업무 리스트</caption>
	                <colgroup>
		                <col style="width:7%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:10%;">
		                <col style="width:auto;">
	                </colgroup>
	                <thead>
						<tr>
							<th class="num">No</th>
							<th>관리 코드</th>
							<th>고객명</th>
							<th>신청금액</th>
							<th>진행 상태</th>
							<th>상담ID</th>
							<th>신청일</th>
							<th>마지막 수정일</th>
							<th>최근 메모</th>
						</tr>
					</thead>
					<tbody>
						<!-- <tr>
							<td>48</td>
							<td><a href="#">A123456</a></td>
							<td>홍○동</td>
							<td>3,000만원</td>
							<td>상담 신청</td>
							<td>consult1</td>
							<td>2016.11.20</td>
							<td>-</td>
							<td>빠른 심사 요청</td>
						</tr> -->
					</tbody>
				</table>
			</div>
			<!-- //lst_table01 -->

			<!-- paging -->
			<div class="paging" id="paging"></div>
			<!-- //paging -->
		</div>

	</div>
	<!-- //container -->

</div>

<!-- layerpopup 대출신청 상세정보 -->
<div class="layerpop_wrap" id="progressDetail" style="display:none;">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:600px; top:100px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">대출신청 상세정보</div>
			
			<div class="clearbox">
				<div class="detail_code">
					<!-- wrt_table01 -->
					<div class="wrt_table01">
						<table border="0" cellspacing="0" cellpadding="0">
			                <caption>관리코드</caption>
			                <colgroup>
				                <col style="width:38%;">
				                <col style="width:62%;">
			                </colgroup>
							<tbody>
								<tr>
									<th>관리코드</th>
									<td id="progressId"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //wrt_table01 -->
				</div>
				<div class="loan_state">
					<button type="button" class="btn_st bt02_1"><span>대출 진행 중지</span></button>
					<!-- <button type="button" class="btn_st bt02"><span>대출 진행 완료</span></button> -->
				</div>
			</div>

			<div class="layer_stit">기본정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출신청 기본정보</caption>
	                <colgroup>
		                <col style="width:19%;">
		                <col style="width:29%;">
		                <col style="width:18%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>고객명</th>
							<td id="user_name"></td>
							<th>연락처</th>
							<td id="tel_number"></td>
						</tr>
						<tr>
							<th>신청일</th>
							<td id="reg_date"></td>
							<th>마지막 수정일</th>
							<td id="modify_date"></td>
						</tr>
						<tr>
							<th>대출신청금액</th>
							<td>
								<div class="clearbox">
									<div class="fl_box" id="require_amount"></div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="amount_change_button"><span>금액변경</span></button></div>
								</div>
							</td>
							<th>상품정보</th>
							<td>
								<div class="clearbox">
									<div class="fl_box" id="eform_info_edit" data-seq="">-</div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="eform_attach_choose_button"><span>상품선택</span></button></div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">진행상태 정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출신청 진행상태 정보</caption>
	                <colgroup>
		                <col style="width:19%;">
		                <col style="width:36%;">
		                <col style="width:15%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>현재 상태</th>
							<td colspan="3" id="status_name"></td>
						</tr>
						<tr>
							<th rowspan="4">세부절차 별<br>진행상태</th>
							<td>
								<div class="clearbox">
									<div class="fl_box">전화 상담</div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="call_change_button"><span>상태변경</span></button></div>
								</div>
							</td>
							<td class="ac" id="call_yn_txt"></td>
							<td id="call_chk_date"></td>
						</tr>
						<tr>
							<td>
								<div class="clearbox">
									<div class="fl_box">전자 서명</div>
									<div class="fr_box">
										<button type="button" class="btn_st bt02"><span>조회</span></button>
										<button type="button" class="btn_st bt02" id="eform_userdata_edit_button"><span>정보등록</span></button>
									</div>
								</div>
							</td>
							<td class="ac" id="eform_yn_txt">-</td>
							<td id="eform_yn_date">-</td>
						</tr>
						<tr>
							<td>계좌 인증</td>
							<td class="ac" id="account_verify_yn_txt">미진행</td>
							<td id="account_verify_date">-</td>
						</tr>
						<tr>
							<td>
								<div class="clearbox">
									<div class="fl_box">서류 제출</div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="eform_attach_button"><span>상태변경</span></button></div>
								</div>
							</td>
							<td class="ac" id="attach_yn_txt">미진행</td>
							<td id="attach_complete_date">-</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->
			<p class="stxt01 ar"><span class="pnt_clr">※ 고객이 사진 촬영 후 등록한 대출 관련 서류는 본 어드민에서 제공되지 않습니다.</span></p>

			<div class="layer_stit">상담이력 메모</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01 memo_scroll">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출신청 상담이력 메모</caption>
	                <colgroup>
		                <col style="width:8%;">
		                <col style="width:auto;">
		                <col style="width:30%;">
	                </colgroup>
					<tbody id="memo_scroll">
						<!-- <tr>
							<th>15</th>
							<td>200만원에서 500만원으로 신청금액 변경</td>
							<td class="ac">2016.11.20 15:30:20</td>
						</tr> -->
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="clearbox memo_addbox">
				<input type="text" name="" id="progressDetail_body" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)">
				<button type="button" class="btn_st bt02" id="progressDetail_body_add"><span>메모추가</span></button>
			</div>

			<div class="layer_btnbox">
				<!-- <button type="button" class="btn_st bt03"><span>수정</span></button> -->
				<button type="button" class="btn_st bt03" id="progressDetailClose"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 대출신청 상세정보 -->

<!-- layerpopup 금융상품 설정 -->
<div class="layerpop_wrap" style="display:none;" id="eform_attach_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:360px; top:200px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">금융상품 설정</div>
			
			<p>고객과 진행 협의된 금융상품을 설정합니다.</p>
			<p style="margin-top:15px;"><input type="checkbox" id="tel01" name="tel01"> 본건으로 고객과 상담 전화 통화 진행</p>
   			<p style="margin-left:20px;">(비대면 거래 진행시 통화 진행 필수)</p>

			<div class="layer_stit">상품 선택</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01 prod_scroll">
				<form>
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>금융상품 상품 선택</caption>
	                <colgroup>
		                <col style="width:19%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody id="eFormInfoList"></tbody>
				</table>
				</form>
			</div>
			<!-- //wrt_table01 -->

			<p class="stxt01">※상품이 등록되어야, 고객이 모바일 웹에 접속한 후 서류 제출 등의 필요한 절차를 진행할 수 있습니다.</p>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="eform_attach_submit" data-flag=""><span>확인</span></button>
				<button type="button" class="btn_st bt03" id="eform_attach_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 금융상품 설정 -->

<!-- layerpopup 대출신청 금액 관리 -->
<div class="layerpop_wrap" style="display:none;" id="require_amount_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:360px; top:220px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">대출신청 금액 관리</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출신청 금액</caption>
	                <colgroup>
		                <col style="width:40%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>대출신청금액</th>
							<td><input type="text" name="" id="require_amount_value" value="" style="width:120px;"> 만원</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">상담이력 메모</div>
			<div><textarea name="" id="require_amount_layer_note" style="width:93%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea></div>

			<p class="stxt01">※대출신청금액 변경은 상담을 위한 관리 자료입니다.<br>
			<span class="pnt_clr">대출시스템과 연계되거나, 실제 대출 금액에 영향을 미치지 않습니다.</span></p>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="require_amount_layer_submit"><span>확인</span></button>
				<button type="button" class="btn_st bt03" id="require_amount_layer_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 대출신청 금액 관리 -->


<!-- layerpopup 전화상담 관리 -->
<div class="layerpop_wrap" style="display:none;" id="call_yn_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:360px; top:220px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">전화상담 관리</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출상담통화 진행상태</caption>
	                <colgroup>
		                <col style="width:40%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>대출상담통화</th>
							<td>
								<select name="" id="call_yn"  style="width:153px;">
									<option value="0">미진행</option>
									<option value="1">진행 완료</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">상담이력 메모</div>
			<div><textarea name="" id="call_yn_layer_note" style="width:93%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea></div>

			<p class="stxt01">※전화상담은 비대면 대출 필수 진행 항목이며, 상담직원이 상태를 업데이트 할 수 있습니다.</p>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="call_yn_submit"><span>확인</span></button>
				<button type="button" class="btn_st bt03" id="call_yn_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 전화상담 관리 -->


<!-- layerpopup 서류 제출 상태 관리 -->
<div class="layerpop_wrap" style="display:none;" id="attach_yn_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:360px; top:220px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">서류 제출 상태 관리</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>서류 진행상태</caption>
	                <colgroup>
		                <col style="width:40%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>진행상태</th>
							<td>
								<select name="" id="eform_attach_status" style="width:153px;">
									<option value="3">서류심사</option>
									<option value="4">서류반려</option>
									<option value="5">대출완료</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->
			
			<div class="layer_stit">재등록 필요 서류</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<form>
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>재등록 필요 서류</caption>
	                <colgroup>
		                <col style="width:19%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody id="eform_attach_list">
					</tbody>
				</table>
				</form>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">상담이력 메모</div>
			<div><textarea name="" id="attach_yn_note" style="width:93%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea></div>

			<p class="stxt01">※제출된 서류 문제로 다시 제출해야 할 경우, 상태 변경 및 문서 선택 후 등록하세요.</p>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="attach_yn_layer_submit"><span>확인</span></button>
				<button type="button" class="btn_st bt03" id="attach_yn_layer_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 서류 제출 상태 관리 -->


<!-- layerpopup 대출신청 등록 -->
<div class="layerpop_wrap" style="display:none;">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:600px; top:270px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">대출신청 등록</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출신청 등록</caption>
	                <colgroup>
		                <col style="width:19%;">
		                <col style="width:29%;">
		                <col style="width:18%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>고객명</th>
							<td><input type="text" name="" style="width:100%;"></td>
							<th>연락처</th>
							<td><input type="text" name="" style="width:100%;"></td>
						</tr>
						<tr>
							<th>대출신청금액</th>
							<td><input type="text" name="" style="width:100%;"></td>
							<th>상품정보</th>
							<td>
								<div class="clearbox">
									<div class="fl_box"></div>
									<div class="fr_box"><button type="button" class="btn_st bt02"><span>상품선택</span></button></div>
								</div>
							</td>
						</tr>
						<tr>
							<th>전화상담</th>
							<td colspan="3">
								<input type="checkbox" id="phone_check" name="phone_check01">
								<label for="phone_check">본 상품과 관련해 고객과 전화상담을 진행했습니다.</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_stit">상담이력 메모</div>
			<div><textarea name="" id="" style="width:96%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea></div>

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03"><span>확인</span></button>
				<button type="button" class="btn_st bt03"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 대출신청 등록 -->

<!-- layerpopup 전자서명 정보 수정 -->
<div class="layerpop_wrap" style="display:none;" id="eform_userdata_edit_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:600px; top:50px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">전자서명 정보 수정</div>
			
			<div class="layer_stit">고객정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>고객정보</caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:30%;">
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>고객명</th>
							<td><input type="text" name="" style="width:120px;" id="edit_user_name" readonly></td>
							<th>연락처</th>
							<td><input type="text" name="" style="width:145px;" id="edit_tel_number" readonly></td>
						</tr>
						<tr>
							<th>주민등록번호</th>
							<td colspan="3"><input type="text" name="" id="edit_jumin" style="width:120px;" readonly> - *******</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<p class="stxt02">※ 고객이 모바일에서 확인하는 전자서명 계약 정보입니다. 정확한 정보를 입력하세요.</p>

			<div class="layer_stit" style="padding-top:0;">대출거래정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출거래정보</caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:30%;">
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>상품명</th>
							<td colspan="3">
								<div class="clearbox">
									<div class="fl_box" id="eform_userdata_edit_eform_info" data-seq=''>모바일대출상품 1</div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="edit_eform_attach_choose_button"><span>상품선택</span></button></div>
								</div>
							</td>
						</tr>
						<tr>
							<th>대출금액</th>
							<td colspan="3"><input type="text" name="" style="width:145px;" id="edit_require_amount"> 만원</td>
						</tr>
						<tr>
							<th>거래 은행명</th>
							<td><input type="text" name="" id="edit_bank" style="width:145px;"></td>
							<th>계좌번호</th>
							<td><input type="text" name="" id="edit_accouunt_number" style="width:145px;"></td>
						</tr>
						<tr>
							<th>여신개시일</th>
							<td colspan="3">
								<input type="text" name="" id="edit_eform_userdata_start_date" class="datepicker" style="width:145px;" placeholder="날짜를 선택하세요.">
								<input type="text" name="" id="edit_eform_userdata_start_date_memo" style="width:265px;" placeholder="간단히 날짜 메모를 입력할 수 있습니다.">
							</td>
						</tr>
						<tr>
							<th>여신기간만료일</th>
							<td colspan="3">
								<input type="text" name="" id="edit_eform_userdata_expire_date" class="datepicker" style="width:145px;" placeholder="날짜를 선택하세요.">
								<input type="text" name="" id="edit_eform_userdata_expire_date_memo" style="width:265px;" placeholder="간단히 날짜 메모를 입력할 수 있습니다.">
							</td>
						</tr>
						<tr>
							<th>이자율</th>
							<td colspan="3">
								<input type="text" name="" id="edit_eform_userdata_interest_per" style="width:145px;" placeholder="ex)고정금리, 변동금리 등">
								<input type="text" name="" id="edit_eform_userdata_interest_per_memo" style="width:265px;" placeholder="이자율을 입력하세요. ex)연 10%">
							</td>
						</tr>
						<tr>
							<th>이자지급시기</th>
							<td colspan="3">
								<input type="text" name="" id="edit_eform_userdata_interest_day" style="width:145px;" placeholder="ex) 매월 5일 등">
							</td>
						</tr>
						<tr>
							<th>상환방법</th>
							<td><input type="text" name="" id="edit_eform_userdata_repayment_method" style="width:145px;"></td>
							<th>중도상환/<br>취급수수료</th>
							<td><input type="text" name="" id="edit_eform_userdata_repayment_commission" style="width:145px;"></td>
						</tr>
						<tr>
							<th>여신실행방법</th>
							<td colspan="3">
								<input type="text" name="" id="edit_eform_userdata_loan_method" style="width:99%;" placeholder="ex) 매월 5일 등">
							</td>
						</tr>
						<tr>
							<th>메모</th>
							<td colspan="3">
								<textarea name="" id="edit_eform_userdata_note" style="width:94%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->
			
			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="eform_userdata_edit_submit"><span>수정</span></button>
				<button type="button" class="btn_st bt03" id="eform_userdata_edit_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 전자서명 정보 수정 -->

<!-- layerpopup 고객 신규등록 -->
<div class="layerpop_wrap" style="display:none;" id="eform_userdata_insert_layer">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:600px; top:50px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">고객 신규등록</div>
			
			<div class="layer_stit">고객정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>고객정보 </caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:30%;">
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>고객명</th>
							<td><input type="text" name="" style="width:120px;" id="insert_user_name"></td>
							<th>연락처</th>
							<td><input type="text" name="" style="width:145px;" id="insert_tel_number"></td>
						</tr>
						<tr>
							<th>주민등록번호</th>
							<td colspan="3"><input type="text" name="" id="insert_jumin" style="width:120px;"> - *******</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->

			<p class="stxt02">※ 고객이 모바일에서 확인하는 전자서명 계약 정보입니다. 정확한 정보를 입력하세요.</p>

			<div class="layer_stit" style="padding-top:0;">대출거래정보</div>
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>대출거래정보</caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:30%;">
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>상품명</th>
							<td colspan="3">
								<div class="clearbox">
									<div class="fl_box" id="eform_userdata_insert_eform_info" data-seq=''></div>
									<div class="fr_box"><button type="button" class="btn_st bt02" id="insert_eform_attach_choose_button"><span>상품선택</span></button></div>
								</div>
							</td>
						</tr>
						<tr>
							<th>대출금액</th>
							<td colspan="3"><input type="text" name="" style="width:145px;" id="insert_require_amount"> 만원</td>
						</tr>
						<tr>
							<th>거래 은행명</th>
							<td><input type="text" name="" id="insert_bank" style="width:145px;"></td>
							<th>계좌번호</th>
							<td><input type="text" name="" id="insert_accouunt_number" style="width:145px;"></td>
						</tr>
						<tr>
							<th>여신개시일</th>
							<td colspan="3">
								<input type="text" name="" id="insert_eform_userdata_start_date" class="datepicker" style="width:145px;" placeholder="날짜를 선택하세요.">
								<input type="text" name="" id="insert_eform_userdata_start_date_memo" style="width:265px;" placeholder="간단히 날짜 메모를 입력할 수 있습니다.">
							</td>
						</tr>
						<tr>
							<th>여신기간만료일</th>
							<td colspan="3">
								<input type="text" name="" id="insert_eform_userdata_expire_date" class="datepicker" style="width:145px;" placeholder="날짜를 선택하세요.">
								<input type="text" name="" id="insert_eform_userdata_expire_date_memo" style="width:265px;" placeholder="간단히 날짜 메모를 입력할 수 있습니다.">
							</td>
						</tr>
						<tr>
							<th>이자율</th>
							<td colspan="3">
								<input type="text" name="" id="insert_eform_userdata_interest_per" style="width:145px;" placeholder="ex)고정금리, 변동금리 등">
								<input type="text" name="" id="insert_eform_userdata_interest_per_memo" style="width:265px;" placeholder="이자율을 입력하세요. ex)연 10%">
							</td>
						</tr>
						<tr>
							<th>이자지급시기</th>
							<td colspan="3">
								<input type="text" name="" id="insert_eform_userdata_interest_day" style="width:145px;" placeholder="ex) 매월 5일 등">
							</td>
						</tr>
						<tr>
							<th>상환방법</th>
							<td><input type="text" name="" id="insert_eform_userdata_repayment_method" style="width:145px;"></td>
							<th>중도상환/<br>취급수수료</th>
							<td><input type="text" name="" id="insert_eform_userdata_repayment_commission" style="width:145px;"></td>
						</tr>
						<tr>
							<th>여신실행방법</th>
							<td colspan="3">
								<input type="text" name="" id="insert_eform_userdata_loan_method" style="width:99%;" placeholder="ex) 매월 5일 등">
							</td>
						</tr>
						<tr>
							<th>전화상담</th>
							<td colspan="3">
								<input type="checkbox" id="phone_check02" name="phone_check02">
								<label for="phone_check02">본 상품과 관련해 고객과 전화상담을 진행했습니다.</label>
							</td>
						</tr>
						<tr>
							<th>메모</th>
							<td colspan="3">
								<textarea name="" id="insert_eform_userdata_note" style="width:94%; height:50px;" placeholder="상담 관리에 필요한 메모 이력을 등록할 수 있습니다.(최대 30자)"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //wrt_table01 -->
			
			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" id="eform_userdata_insert_submit"><span>등록</span></button>
				<button type="button" class="btn_st bt03" id="eform_userdata_insert_close"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 고객 신규등록 -->
</body>
</html>