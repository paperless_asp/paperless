<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
<script type="text/javascript">
var pageSize = 10;

$(document).ready(function(){
	getViewAjax();
	getSelectBox();
});

function getSelectBox(){
	var method="POST";
	var requestUrl="/${map.loanId}/api/loanInfoSelectList";
	var params = {};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(response) {
			if(response.result){
				$("#search_option1").empty();
				$("#search_option1").append("<option value=''>회사선택</option>");
				$(".search_option1").find('strong').append('회사선택');
				$(response.list).each(function(k,v){
					$("#search_option1").append("<option value=\""+v.id+"\">"+v.name+"</option>");
				});
			}
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
}

function getViewAjax(){
	
	var pageNo = $("#pageNo").val();
	var searchOption1 = $("#search_option1").val();
	var searchOption2 = $(".search_option2").val();
	var searchWord = $(".search_word").val();
	var totalCnt = 0;
	var method="POST";
	var requestUrl="/${map.loanId}/api/noticeSelectList";
	var params = {
		"pageNo": pageNo, 
		"pageSize": pageSize,
		"searchOption1" : searchOption1,
		"searchOption2" : searchOption2,
		"searchWord" : searchWord
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(response) {
			var pageBlock = 10;
			var pageNo = $("#pageNo").val();
			var stateNo = (pageNo-1) * pageSize;
			if (response.result) {
				$(".lst_table01 tbody").empty();
				$(response.list).each(function(k,v){
					totalCnt = v.totalCnt;
					if(stateNo % 2 == 0){
						$(".lst_table01 tbody").append("<tr>");
					}else{
						$(".lst_table01 tbody").append("<tr class=\"bg\">");
					}
					$(".lst_table01 tbody").append(
							"<td><input type=\"checkbox\" name=\"prod_01\" value=\""+v.seq+"\"></td>"+
							"<td class=\"al\">"+v.loan_id+"</td>"+
							"<td class=\"al\" id=\""+v.seq+"\"><a href=\"javascript:notice_detail_function('"+v.seq+"')\">"+v.title+"</a></td>"+
							"<td>"+v.admin_id+"</td>"+
							"<td>"+v.reg_date+"</td>"+
						"</tr>"
					);
					stateNo = stateNo + 1;
				});
				//카운트
				$(".txt").text("총 "+totalCnt+"건의 결과가 검색되었습니다.");
				//페이징 세팅
				var html = setPaging(pageNo , pageSize , pageBlock , totalCnt);
				
				$("#paging").empty();
				$("#paging").append(html);
			} else {
				alert("실패");
			}
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
}

function search(){
	
	var searchWord = $(".search_word").val();
	
	if(searchWord == "")
	{
		alert('검색어를 입력해 주세요.')
		return;
	}
	getViewAjax();
}

function notice_detail_function(seq){

	var method="POST";
	var requestUrl="/${map.loanId}/api/noticeSelectOne";
	var params = {
		"seq": seq
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(response) {
			if (response.result) {
				var map = response.map;
				$('.modify_title').val(map.title);
				$('.modify_body').val(map.body);
				$('.modify_seq').val(map.seq);
			} else {
				alert("실패");
			}
			
			
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
	
	$(".notice_modify_layer").attr("style","display:block;");
}

function update_notice()
{
	var method="POST";
	
	var title = $('.modify_title').val();
	var body = $('.modify_body').val();
	var seq = $('.modify_seq').val();
	
	var requestUrl="/${map.loanId}/api/noticeUpdate";
	var params = {
		"seq": seq,
		"title" : title,
		"body" : body
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(response) {
			if (response.result) {
				//alert('#'+seq+" a");
				location.reload();
				//$('#'+seq+" a").innerHTML = title;
				//$(".notice_modify_layer").attr("style","display:none;");
			} else {
				alert("실패");
			}
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
	
	
}
function add_notice_layer_open()
{
	$(".add_notice_layer").attr("style","display:block;");
}
function modify_layer_close()
{
	$(".notice_modify_layer").attr("style","display:none;");
}

function add_notice_layer_close()
{
	$(".add_notice_layer").attr("style","display:none;");
}

</script>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
		<!-- hidden -->
		<input type="hidden" id="pageNo" value="1" />
	<!-- //header -->

	<!-- container -->
	<div id="container">
		<!-- sub menu -->
		<%@ include file="/WEB-INF/views/include_web/sub_menu.jsp"%>
		<!-- sub search -->
		<div class="sub_topsearch">
			<ul class="clearbox">
				<li>
					<select name="" id="search_option1" class="select01 search_option1" style="width:153px;">
					</select>
				</li>
				<li>
					<select name="" id="" class="select01 search_option2" style="width:153px;">
						<option value="title">제목</option>
						<option value="body">내용</option>
					</select>
				</li>
				<li class="input_search"><input class="search_word" type="text" name="" placeholder="검색어를 입력하세요"></li>
				<li><button type="button" class="btn_st bt01" onclick="javascript:search()"><span>검색</span></button></li>
			</ul>
		</div>

		<!-- top btn -->
		<div class="top_area clearbox">
			<div class="lf_box">
				<button type="button" class="btn_st bt01"><span>삭제</span></button>
			</div>
			<div class="rf_box">
				<button type="button" class="btn_st bt01" onclick="javascript:add_notice_layer_open()"><span>공지 등록</span></button>
			</div> 
		</div>

		<!-- contents -->
		<div class="contents">
			<!-- lst_table01 -->
			<div class="lst_table01">
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>공지사항 리스트</caption>
	                <colgroup>
		                <col style="width:6%;">
		                <col style="width:15%;">
		                <col style="width:auto;">
		                <col style="width:12%;">
		                <col style="width:12%;">
	                </colgroup>
	                <thead>
						<tr>
							<th class="num"><input type="checkbox" name="notice_all"></th>
							<th>구분</th>
							<th>제목</th>
							<th>등록ID</th>
							<th>등록일</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- //lst_table01 -->

			<!-- paging -->
			<div class="paging" id="paging"></div>
			<!-- //paging -->
		</div>

	</div>
	<!-- //container -->

</div>


<!-- layerpopup 공지사항 등록 -->
<div class="layerpop_wrap add_notice_layer" style="display:none;">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:500px; top:200px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">공지사항 등록</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<form>
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>공지사항 등록</caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<tr>
							<th>제목</th>
							<td><input type="text" name="" style="width:100%;" placeholder="공지 제목을 입력하세요."></td>
						</tr>
						<tr>
							<th>내용</th>
							<td><textarea name="" id="" style="width:93%; height:150px;" placeholder="공지 내용을 입력하세요."></textarea></td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03"><span>등록</span></button>
				<button type="button" class="btn_st bt03" onclick="javascript:add_notice_layer_close()"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 공지사항 등록 -->

<!-- layerpopup 공지사항 수정 -->
<div class="layerpop_wrap notice_modify_layer" style="display:none;">
	<div class="layerpop_mask"></div>
	<div class="layerpop" style="width:500px; top:200px;">
		<div class="layer_rdbox">
			<div class="layer_lt"></div>
			<div class="layer_rt"></div>
		</div>
		<div class="layer_content">
			<div class="layer_tit">공지사항 수정</div>
			
			<!-- wrt_table01 -->
			<div class="wrt_table01">
				<form>
				<table border="0" cellspacing="0" cellpadding="0">
	                <caption>공지사항 등록</caption>
	                <colgroup>
		                <col style="width:20%;">
		                <col style="width:auto;">
	                </colgroup>
					<tbody>
						<input class="modify_seq" type="hidden" name="" style="width:100%;" value="">
						<tr>
							<th>제목</th>
							<td><input class="modify_title" type="text" name="" style="width:100%;" value="공지 제목을 입력하세요."></td>
						</tr>
						<tr>
							<th>내용</th>
							<td><textarea class="modify_body" name="" id="" style="width:93%; height:150px;">공지 내용을 입력하세요.</textarea></td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
			<!-- //wrt_table01 -->

			<div class="layer_btnbox">
				<button type="button" class="btn_st bt03" onclick="javascript:update_notice()"><span>수정</span></button>
				<button type="button" class="btn_st bt03" onclick="javascript:modify_layer_close()"><span>취소</span></button>
			</div>
		</div>

		<div class="layer_rdbox">
			<div class="layer_lb"></div>
			<div class="layer_rb"></div>
		</div>
	</div>
</div>
<!-- //layerpopup 공지사항 수정 -->

</body>
</html>