$(document).ready(function(){
	$("#fast_check").bind("click",function(){
		$("#call_start_time option").eq(0).attr("selected","selected");
		$("#call_end_time option").eq(23).attr("selected","selected");
	});
	
	$("#faseBtn").bind("click",function(){
		if(validation()){
			$.blockUI({ 
				/*css: {
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	        	}*/
				message :
					"<div class='loading_box' style='display:block;'><div class='mask_area'></div>"+
					"<div class='loading_cont'><img src='/static/img/loading_1.gif'/></div>"
			}); 
	        setTimeout($.unblockUI, 2000); 
			//fast_app();
		}
	});
});

function validation(){
	if(!onlyHan("user_name" , "한글만 가능합니다.")){
		return false;
	}
	if(!checkVal("tel_number")){
		return false;
	}
	if(!checkVal("amount")){
		return false;
	}
	if(!$("#fast_check").prop("checked")){
		if($("#call_start_time option:selected").val() >= $("#call_end_time option:selected").val()){
			alert("희망상담시간을 확인해주십시오.");
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function fast_app(){
	//대출신청 저장
	//${map.loanId }
	var method="POST";
	var requestUrl="http://localhost:8080/api/setProgress";
	var params = {
		"user_name": $("#user_name").val(), 
		"loan_id": $("#loanId").val(), 
		"tel_number":$("#tel_number").val(), 
		"require_amount" : $("#amount").val(),
		"call_start_time":$("#call_start_time option:selected").val(), 
		"call_end_time":$("#call_end_time option:selected").val()
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType , contType , resultData);
}

function resultData (response) {
	if(response.message != "")
		alert(response.message);
	/*if(response.result){
		window.location.href="step01";
	}*/
}


/*
 * method : POST , GET
 * getType : HTML JSON XML
 * 
 */
function ajaxGetData(method , requestUrl , params ,getType ,contType , functionObj){
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(result) {
			if (functionObj) {
				functionObj(result);
			} else {
				return result;
			}
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
 }

function ajaxGetData_get(method , requestUrl , params ,getType ){

	$.ajax({
		url: requestUrl,
		type: method,
		data: params,
		dataType: getType,
		cache: false,
		success: function(result) {
			//alert(result.responseText);
			return result;
		},
		fail: function() {
			alert("서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
 }


//한글만입력
function onlyHan(id , msg)
{
	if(!checkVal(id)){
		return false;
	}
	
	regexp = /[a-z0-9]|[ \[\]{}()<>?|`~!@#$%^&*-_+=,.;:\"'\\]/g;
	v = $("#"+id).val();
	if( regexp.test(v) ) {
		alert(msg);
		$("#"+id).val(v.replace(regexp,''));
		return false;
	}else{
		return true;
	}
}

//값 확인
function checkVal(id){
	if($("#"+id).val() == ""){
		alert("값을 입력해주세요.");
		return false;
	}else{
		return true;
	}
}


//숫자만 입력
function setNum(obj){
	val=$("#" + obj).val();
	re=/[^0-9]/gi;
	$("#" + obj).val(val.replace(re,""));
}

function setPaging(pageNo , pageSize , pageBlock , totalCnt){
	var html = "";
	if(totalCnt <= pageSize){
		html += "<a href=\"javascript:setPage(1)\" class=\"first\"><img src=\"/static/img/common/btn_paging_first.gif\" alt=\"처음\" /></a>";
		html += "<a href=\"javascript:setPage(1)\" class=\"prev\"><img src=\"/static/img/common/btn_paging_prev.gif\" alt=\"이전\" /></a>";
		html += "<a href=\"javascript:setPage(1)\" >1</a>";
		html += "<a href=\"javascript:setPage(1)\" class=\"next\"><img src=\"/static/img/common/btn_paging_next.gif\" alt=\"다음\" /></a>";
		html += "<a href=\"javascript:setPage(1)\" class=\"last\"><img src=\"/static/img/common/btn_paging_last.gif\" alt=\"마지막\" /></a>";
	}else{
		var lastPage = parseInt((totalCnt / pageSize) +1);
		
		html += "<a href=\"javascript:setPage(1)\" class=\"first\"><img src=\"/static/img/common/btn_paging_first.gif\" alt=\"처음\" /></a>";
		if(pageNo > 1)
			html += "<a href=\"javascript:setPage("+(parseInt(pageNo)-1)+")\" class=\"prev\"><img src=\"/static/img/common/btn_paging_prev.gif\" alt=\"이전\" /></a>";
		else
			html += "<a href=\"javascript:setPage(1)\" class=\"prev\"><img src=\"/static/img/common/btn_paging_prev.gif\" alt=\"이전\" /></a>";
		
		for(i=1;i<=lastPage;i++){
			if(i == pageNo)
				html += "<a href=\"javascript:setPage("+i+")\" class=\"on\">"+i+"</a>";
			else
				html += "<a href=\"javascript:setPage("+i+")\" >"+i+"</a>";
		}
		
		if(pageNo < lastPage)
			html += "<a href=\"javascript:setPage("+(parseInt(pageNo)+1)+")\" class=\"next\"><img src=\"/static/img/common/btn_paging_next.gif\" alt=\"다음\" /></a>";
		else
			html += "<a href=\"javascript:setPage("+pageNo+")\" class=\"next\"><img src=\"/static/img/common/btn_paging_next.gif\" alt=\"다음\" /></a>";
		
		html += "<a href=\"javascript:setPage("+lastPage+")\" class=\"last\"><img src=\"/static/img/common/btn_paging_last.gif\" alt=\"마지막\" /></a>";
	}
	
	return html;	
}



function setPage(pageNo){
	$("#pageNo").val(pageNo);
	/*
	var pageSize = $("#pageSize option:selected").val();
	var pageNo = $("#pageNo").val();
	var pageBlock = 10;*/
	getViewAjax();
}


/*
 * 기본 날짜 지정
 */
function setDefaultDate(type){
	var today = new Date();
	var year = today.getFullYear();
	var month = today.getMonth()+1;
	var day = today.getDate();
	if(month < 10) month = "0"+month;
	if(day < 10) day = "0"+day;
	
	var yymmdd = year+"-"+month+"-"+day;

	var today2 = new Date(year,Number(month-1),Number(day)-type);
	var year2 = today2.getFullYear();
	var month2 = today2.getMonth()+1;
	var day2 = today2.getDate();
	if(month2 < 10) month2 = "0"+month2;
	if(day2 < 10) day2 = "0"+day2;
	
	var yymmdd2 = year2+"-"+month2+"-"+day2;
	$("#sdate").val(yymmdd2);
	$("#edate").val(yymmdd);
}